Feature: Token administration
  Scenario: Create tokens
    Given a customer with id "cid1"
    When the tokens are created
    Then the customer has 6 tokens

  Scenario: Validate token
    Given a customer with id "cid1"
    When the tokens are created
    Then the tokens are validated by returning the customer with id "cid1"

  Scenario: Remove one token
    Given a customer with id "cid1"
    When the tokens are created
    And one token is presented
    Then the tokenList has 5 tokens

  Scenario: User present invalid token
    Given a customer with id "cid1"
    When an invalid token is presented
    Then the token is invalid

  Scenario: Request token
    Given a customer with id "cid1"
    When the tokens are created
    And the customer spends 5 tokens
    Then the customer has 1 tokens
    Then the customer requests 4 tokens
    Then the customer has 5 tokens

  Scenario: Generate tokens for token request
    Given a customer with id "cid1"
    When the TokenGenerationRequested event is received
    Then the "CustomerTokenAssigned" event is sent
    Then the customer has 6 tokens

  Scenario: Customer tokens removal based on Unregister request
    Given a customer with id "cid1"
    When the tokens are created
    When the UnregisterCustomerRequested event is received
    Then the CustomerTokensRemoved event is sent
    Then the customer has no tokens

  Scenario: Retrieve cid from token request
    Given a customer with id "cid1"
    When the tokens are created
    Then the customer has 6 tokens
    When the TokenCIDRequested event is received
    Then the CIDReturned event is sent
    Then the customer has 5 tokens





