package token;
import Modules.*;
import java.util.HashMap;

public class TokenRepository {
    //Token ,cid
    private HashMap<String,String> tokens;
    //cid ,token count
    private HashMap<String, Integer> tokensCount = new HashMap<>();

    public TokenRepository() {

        tokens = new HashMap<>();
        tokensCount = new HashMap<>();
    }



    public HashMap<String,Integer> getCountMap(){
        return tokensCount;
    }
    public HashMap<String,String> getTokensMap(){
        return tokens;
    }


    public void addToken(String token, String cid){
        tokens.put(token,cid);
    }

    public void addTokenCount(String cid, int count){
        tokensCount.put(cid,count);
    }

    public void removeToken(String token){
        tokens.remove(token);
    }

    public void reduceToken(String cid){
        int currentCount = tokensCount.get(cid);
        tokensCount.put(cid,currentCount - 1);

    }


}
