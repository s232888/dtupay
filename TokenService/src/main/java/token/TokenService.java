package token;

import Modules.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.*;


public class TokenService {

    private TokenRepository tokenRepository = new TokenRepository();
    MessageQueue queue;

    public TokenService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("TokenGenerationRequested", this::handleTokenGenerationRequested);
        this.queue.addHandler("TokenRemovalRequested", this::handleTokenRemovalRequested);
        this.queue.addHandler("TokenToCIDRequested", this::handleTokenToCIDRequested);
        this.queue.addHandler("GetCustomerTokens", this::handleGetCustomerTokens);
    }

    private void handleGetCustomerTokens(Event ev) {
        System.out.println("Get Customer Tokens event caught");
        var customer = ev.getArgument(0, Customer.class);
        // Do something
        String cid = customer.getId();
        Integer amount = customer.getTokensRequestedAmount();
        var customerTokens = generateTokens(cid,amount);
        System.out.println("CustomerTokensReturned event raised");
        Event event = new Event("CustomerTokensReturned", new Object[] { customerTokens });
        queue.publish(event);
    }

    public void handleTokenGenerationRequested(Event ev) {
        System.out.println("Token Generation Requested event caught");
        var customer = ev.getArgument(0, Customer.class);

        List<String> tokensList = generateTokens(customer.getId(),6);
        customer.setTokens(tokensList);
        System.out.println("Customer Token Assigned event raised");
        Event event = new Event("CustomerTokenAssigned", new Object[] { customer });
        queue.publish(event);
    }

    public void handleTokenRemovalRequested(Event ev) {
        System.out.println("Token Removal Requested event caught");
        var cid = ev.getArgument(0, String.class);
        System.out.println(cid);
        boolean customerRemoved = true;
        List<String> tokensForCID = new ArrayList<>();
        tokenRepository.getTokensMap().forEach((key,value) -> {
            if (value.equals(cid)) {
                tokensForCID.add(key);
            }
        });

        for (int i = 0; i < tokensForCID.size(); i++){
            tokenRepository.removeToken(tokensForCID.get(i));
            tokenRepository.reduceToken(cid);
        }

        if (tokenRepository.getTokensMap().containsValue(cid)){
            customerRemoved = false;
        }
        System.out.println("Customer Token Removed event raised");
        Event event = new Event("CustomerTokenRemoved", new Object[] { customerRemoved });
        queue.publish(event);
    }

    public void handleTokenToCIDRequested(Event ev) {
        var token = ev.getArgument(0, String.class);
        System.out.println(token);
        Event event = new Event("CIDReturned", new Object[] { validateToken(token) });
        queue.publish(event);
    }


    public List<String> generateTokens(String cid, int amount){ // generates amount of tokens for the user
        List<String> customerTokens = new ArrayList<>();

        tokenRepository.getTokensMap().forEach((key,value) -> {
            if (value.equals(cid)) {
                customerTokens.add(key);
            }
        });

        if(!tokenRepository.getCountMap().containsKey(cid)) {
            System.out.println("Customer not in repository yet");
            tokenRepository.addTokenCount(cid, 0);
        }

        System.out.println("count map: "+tokenRepository.getCountMap().get(cid));
        if (tokenRepository.getCountMap().get(cid) <= 1 && amount +
                tokenRepository.getCountMap().get(cid) <= 6){
            for(int i = 0; i < amount; i++){
                String token = String.valueOf(UUID.randomUUID());

                tokenRepository.addToken(token,cid);
                customerTokens.add(token);
            }
            tokenRepository.addTokenCount(cid,amount +
                    tokenRepository.getCountMap().get(cid));
        }
        return customerTokens;
    }

    public int getTokenCount(String cid){
       return tokenRepository.getCountMap().get(cid);
    }
    public String getCID(String token){
        return tokenRepository.getTokensMap().get(token);
    }

    public String validateToken(String token){
        System.out.println("Tokens available: ");
        System.out.println(tokenRepository.getTokensMap().keySet());
        if(tokenRepository.getTokensMap().containsKey(token)){
            String cid = getCID(token);
            // Remove token from tokensMap
            tokenRepository.removeToken(token);
            // Reduce count for cid from CountMap
            tokenRepository.reduceToken(cid);
        return cid;
        }
        return "Invalid Token";
    }


}
