package token;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import Modules.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;


import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;

import org.mockito.ArgumentCaptor;
import static org.junit.Assert.*;

public class TokenTestSteps {
    // MQ
    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();

    MessageQueue queue = mock(MessageQueue.class);
    TokenService tokenService = new TokenService(queue);

    Customer newCustomer;

    /*private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
            publishedEvent.complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };*/
    //    private AccountService service = new AccountService(q);
    //---------
    Customer customer = new Customer("Juan", "Test", "101010-9999");
    Customer expected;
    String cid,invalidToken, invalidTokenResult;
    List<String> tokenList = new ArrayList<>();
    int tokenCount;
    int tokenAmount;

    @Given("a customer with id {string}")
    public void aCustomerWithId(String cid) {
        this.cid = cid;
        customer.setId(cid);
    }

    @When("the tokens are created")
    public void theTokensAreCreated() {
        tokenList = tokenService.generateTokens(cid, 6);
    }

    @Then("the customer has {int} tokens")
    public void theTokensAreAssignedToCustomerWithId(int numTokens) {
        int tokenCount = tokenService.getTokenCount(cid);
        assertEquals(numTokens, tokenCount);
    }

    @Then("the tokens are validated by returning the customer with id {string}")
    public void theTokensAreValidatedByReturningTheCustomerWithId(String string) {
        for (int i = 0; i < tokenList.size(); i++){
            assertEquals(string,tokenService.getCID(tokenList.get(i)));
        }
    }

    @And("one token is presented")
    public void oneTokenIsPresented() {
        tokenCount = tokenList.size();
        tokenService.validateToken(tokenList.get(0));
    }

    @Then("the tokenList has {int} tokens")
    public void theTokenListHasTokens(int numTokens) {
        assertEquals(numTokens, tokenService.getTokenCount(cid) );
    }

    @When("an invalid token is presented")
    public void anInvalidTokensIsPresented() {
        invalidToken = String.valueOf(UUID.randomUUID());
        invalidTokenResult = tokenService.validateToken(invalidToken);
    }

    @Then("the token is invalid")
    public void theTokenIsInvalid() {
        assertEquals("Invalid Token",invalidTokenResult);
    }

    @Given("a customer with cid {string} with {int} token")
    public void aCustomerWithCidWithToken(String cid, int tokenAmount) {
        this.cid = cid;
        this.tokenAmount = tokenAmount;
    }

    @When("the customer requests {int} tokens")
    public void theCustomerRequestsTokens(int numTokens) {
        tokenService.generateTokens(cid, numTokens);
    }

    @And("the customer spends {int} tokens")
    public void theCustomerSpendsTokens(int numTokens) {
        for (int i = 0; i < numTokens; i++) {
            tokenService.validateToken(tokenList.get(i));
        }
        System.out.println(tokenService.getTokenCount(cid));
    }


    @When("the TokenGenerationRequested event is received")
    public void theTokenGenerationRequestedEventIsReceived() {
        newCustomer = new Customer("Daniel","M","101010101");
        newCustomer.setId(cid);
        tokenService.handleTokenGenerationRequested(new Event("TokenGenerationRequested",new Object[] {newCustomer}));
    }

    @Then("the {string} event is sent")
    public void theEventIsSent(String eventName) {
        expected = new Customer("Daniel","M","101010101");
        Event event = new Event(eventName, new Object[] {expected});
        verify(queue).publish(event);
    }

    @When("the TokenCIDRequested event is received")
    public void theTokenCIDRequestedEventIsReceived() {
        String token = tokenList.get(0);
        tokenService.handleTokenToCIDRequested(new Event("TokenToCIDRequested",new Object[] {token}));
    }

    @Then("the CIDReturned event is sent")
    public void theCIDReturnedEventIsSent() {
        Event event = new Event("CIDReturned", new Object[] {"cid1"});
        verify(queue).publish(event);
    }

    @When("the UnregisterCustomerRequested event is received")
    public void theUnregisterCustomerRequestedEventIsReceived() {
        tokenService.handleTokenRemovalRequested(new Event("TokenRemovalRequested",new Object[] {cid}));
    }

    @Then("the customer has no tokens")
    public void theCustomerHasNoTokens() {
        assertEquals(0, tokenService.getTokenCount(cid));
    }

    @Then("the CustomerTokensRemoved event is sent")
    public void theCustomerTokensRemovedEventIsSent() {
        Event event = new Event("CustomerTokenRemoved", new Object[] {true});
        verify(queue).publish(event);
    }
}
