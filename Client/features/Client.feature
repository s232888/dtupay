Feature: DTU Pay

  Scenario: Register Customer
    Given a customer with name "Michael Jordan" and CPR "5019890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token

  Scenario: Register Merchant
    Given a merchant with name "Jordan Michael" and CPR "4010890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a merchant
    Then the merchant is created

  Scenario: Unregister Merchant
    Given a merchant with name "Jordan Michael" and CPR "4010890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a merchant
    And the merchant unregisters
    Then the merchant no longer exists

  Scenario: Unregister Customer
    Given a customer with name "Michael Jordan" and CPR "5019890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    And the customer unregisters
    Then the customer no longer exists


  Scenario: A successful a transaction between a merchant and a customer
    Given a customer with name "Michael Jordan" and CPR "5019890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    Given a merchant with name "Jordan Michael" and CPR "4010890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a merchant
    And an amount of 100 kr
    When the merchant starts the transaction
    Then the transaction is completed and reported

  Scenario: Customer requests 5 tokens when only 1 remaining
    Given a customer with name "Michael Jordan" and CPR "5019890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    Given a merchant with name "Jordan Michael" and CPR "4010890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a merchant
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    Then the customer has 1 tokens
    When the customer requests 5 new tokens
    Then the customer has 6 tokens

  Scenario: Customer requests 5 tokens when 2 remaining
    Given a customer with name "Michael Jordan" and CPR "5019890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    Given a merchant with name "Jordan Michael" and CPR "4010890-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a merchant
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    When an amount of 50 kr
    And the merchant starts the transaction
    Then the transaction is completed and reported
    Then the customer has 2 tokens
    When the customer requests 5 new tokens
    Then the customer has 2 tokens

  Scenario: Merchant requests a report
    Given a merchant with name "Jordan Michael" and CPR "5019894-9554" and an existing bank account with 1000 dkk
    And a customer with name "Michael Jordan" and CPR "4010896-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    When that person registers themselves as a merchant
    And an amount of 100 kr
    When the merchant starts the transaction
    Then the transaction is completed and reported
    When the merchant requests a report with "mid1"
    Then a report is generated for the merchant

  Scenario: Customer requests a report
    Given a merchant with name "Jordan Michael" and CPR "5019894-9554" and an existing bank account with 1000 dkk
    And a customer with name "Michael Jordan" and CPR "4010896-9554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    When that person registers themselves as a merchant
    And an amount of 100 kr
    When the merchant starts the transaction
    Then the transaction is completed and reported
    When the customer requests a report with "cid1"
    Then a report is generated for the customer

  Scenario: Manager requests a report
    Given a merchant with name "Jordan Michael" and CPR "5019894-5554" and an existing bank account with 1000 dkk
    And a customer with name "Michael Jordan" and CPR "4010896-5554" and an existing bank account with 1000 dkk
    When that person registers themselves as a customer
    Then the customer is created and has a token
    When that person registers themselves as a merchant
    And an amount of 100 kr
    When the merchant starts the transaction
    Then the transaction is completed and reported
    When the manager requests a report
    Then a report is generated for the manager

  # Merchant and customer get only reports on their own payments, manager gets everything