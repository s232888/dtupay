package client;

import Modules.*;
import Modules.Payment;
import jakarta.ws.rs.*;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

import java.math.BigDecimal;
import java.util.List;

public class ClientTest {
    Client c = ClientBuilder.newClient();
    WebTarget r =
            c.target("http://localhost:8080/");

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Customer registerCustomer(Customer customer) {
        return r.path("customers").request(MediaType.APPLICATION_JSON).post(Entity.json(customer), Customer.class);
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Payment sendPayment(Payment p){
        return r.path("payments").request(MediaType.APPLICATION_JSON).post(Entity.json(p), Payment.class);
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Merchant registerMerchant(Merchant merchant) {
        return r.path("merchants").request(MediaType.APPLICATION_JSON).post(Entity.json(merchant), Merchant.class);
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean unregisterCustomer(Customer customer) {
        return r.path("customers/unregister").request(MediaType.APPLICATION_JSON).post(Entity.json(customer), boolean.class);
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean unregisterMerchant(Merchant merchant) {
        return r.path("merchants/unregister").request(MediaType.APPLICATION_JSON).post(Entity.json(merchant), boolean.class);
    }

    @GET
    @Consumes("application/json")
    public Report requestReportMerchant(String mid) {
        return r.path("reports/" + mid).request(MediaType.APPLICATION_JSON).get(Report.class);
    }

    @GET
    @Consumes("application/json")
    public Report requestReportCustomer(String cid) {
        return r.path("reports/" + cid).request(MediaType.APPLICATION_JSON).get(Report.class);
    }

    @GET
    @Consumes("application/json")
    public List<Payment> requestReportManager() {
        return r.path("reports/manager").request(MediaType.APPLICATION_JSON).get(List.class);
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> requestTokens(Customer customer) {
        return r.path("customers/tokensRequest").request(MediaType.APPLICATION_JSON).post(Entity.json(customer), List.class);
    }
}
