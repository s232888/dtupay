package client;


import Modules.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ClientTestSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    Customer customer = new Customer();
    Merchant merchant = new Merchant();
    ClientTest clientTest = new ClientTest();
    private String cid;
    private String mid;
    private int amount;
    private Customer resultingCustomer;
    private Merchant resultingMerchant;
    private String validToken;
    String customerBankAccount, merchantBankAccount, paymentSuccessful, paymentId;
    boolean customerUnregisteredResult,merchantUnregisteredResult;
    Report reportMerchantResult, reportCustomerResult;
    List<Payment> reportManagerResult;

    List<String> accountsList = new ArrayList<>();
    List<String> tokensList = new ArrayList<>();

    @After
    public void after(Scenario scenario) throws BankServiceException_Exception {
        String scenarioName = scenario.getName();
        for (int i = 0; i < accountsList.size(); i++){
            bank.retireAccount(accountsList.get(i));
            System.out.println("Account deleted");
        }
        accountsList.clear();
//        bank.retireAccount(customerBankAccount);
//        bank.retireAccount(merchantBankAccount);
    }

    @Given("a customer with id {string} registered in DTUPay")
    public void aCustomerWithIdRegisteredInDTUPay(String cid) {
        // check that customer exists in DTU Pay
        this.cid = cid;
    }

    @And("a merchant with id {string} registered in DTUPay")
    public void aMerchantWithIdRegisteredInDTUPay(String mid) {
        // check that merchant exists in DTU pay
        this.mid = mid;
    }

    @Given("a merchant with id {string} with {int} kr in his bank account")
    public void aMerchantWithIdWithKrInHisBankAccount(String arg0, int arg1) throws BankServiceException_Exception {
        User userMerchant = new User();
        userMerchant.setFirstName("Michael");
        userMerchant.setLastName("Test");
        userMerchant.setCprNumber("101019-1010");
        merchantBankAccount = bank.createAccountWithBalance(userMerchant, BigDecimal.valueOf(1000));
        accountsList.add(merchantBankAccount);
    }

    @And("a customer token with {int} kr in his bank account")
    public void aCustomerTokenWithKrInHisBankAccount(int arg0) throws BankServiceException_Exception {
        User userCustomer = new User();
        userCustomer.setFirstName("Daniel");
        userCustomer.setLastName("Test");
        userCustomer.setCprNumber("111119-1010");
        customerBankAccount = bank.createAccountWithBalance(userCustomer, BigDecimal.valueOf(1000));
        accountsList.add(customerBankAccount);
    }

    @And("an amount of {int} kr")
    public void anAmountOfKr(int amount) {
        this.amount = amount;
    }

    @When("the merchant starts the transaction")
    public void theMerchantStartsTheTransaction() {
        String token = resultingCustomer.useToken();
        Payment payment = new Payment(BigDecimal.valueOf(amount), token, merchant.getId());
        paymentId = payment.getPaymentId();
//        System.out.println(payment.getCustomerToken());
        paymentSuccessful = clientTest.sendPayment(payment).getPaymentId();
    }

    @Then("the transaction is completed and reported")
    public void theTransactionIsCompletedAndReported() {
        assertEquals(paymentId, paymentSuccessful);
    }


    @Given("a customer with name {string} and CPR {string} and an existing bank account with {int} dkk")
    public void a_customer_with_name_and_cpr_and_an_existing_bank_account_with_dkk(String string, String string2, Integer int1) throws BankServiceException_Exception {
        User userCustomer = new User();
        userCustomer.setFirstName(string.split(" ")[0]);
        userCustomer.setLastName(string.split(" ")[string.split(" ").length - 1]);
        userCustomer.setCprNumber(string2);
        customerBankAccount = bank.createAccountWithBalance(userCustomer, BigDecimal.valueOf(int1));

        customer = new Customer(string.split(" ")[0],string.split(" ")[1],string2, customerBankAccount);
        accountsList.add(customerBankAccount);

    }

    @And("a merchant with name {string} and CPR {string} and an existing bank account with {int} dkk")
    public void aMerchantWithNameAndCPRAndAnExistingBankAccountWithDkk(String string, String string2, int int1) throws BankServiceException_Exception {
        User userMerchant = new User();
        userMerchant.setFirstName(string.split(" ")[0]);
        userMerchant.setLastName(string.split(" ")[string.split(" ").length - 1]);
        userMerchant.setCprNumber(string2);
        merchantBankAccount = bank.createAccountWithBalance(userMerchant, BigDecimal.valueOf(int1));

        merchant = new Merchant(string.split(" ")[0],string.split(" ")[1],string2, merchantBankAccount);
        accountsList.add(merchantBankAccount);
    }

    @When("that person registers themselves as a customer")
    public void that_person_registers_themselves_as_a_customer() {
        resultingCustomer = clientTest.registerCustomer(customer);
    }

    @Then("the customer is created and has a token")
    public void the_customer_is_created_and_has_a_token() {
        validToken = resultingCustomer.getTokens().get(0);
        Assert.assertNotNull(resultingCustomer);
        Assert.assertNotNull(validToken);
    }

    @When("that person registers themselves as a merchant")
    public void thatPersonRegistersThemselvesAsAMerchant() {
        resultingMerchant = clientTest.registerMerchant(merchant);
    }

    @Then("the merchant is created")
    public void theMerchantIsCreated() {
        assertEquals(merchant.getId(), resultingMerchant.getId());
    }

    @And("the customer unregisters")
    public void theCustomerUnregisters() {
        customerUnregisteredResult = clientTest.unregisterCustomer(customer);
    }

    @Then("the customer no longer exists")
    public void theCustomerNoLongerExists() {
        assertTrue(customerUnregisteredResult);
    }

    @And("the merchant unregisters")
    public void theMerchantUnregisters() {
        merchantUnregisteredResult = clientTest.unregisterMerchant(merchant);
    }

    @Then("the merchant no longer exists")
    public void theMerchantNoLongerExists() {
        assertTrue(merchantUnregisteredResult);
    }

    @When("the merchant requests a report with {string}")
    public void theMerchantRequestsAReport(String mid) {
        reportMerchantResult = clientTest.requestReportMerchant(mid);
    }

    @When("the customer requests a report with {string}")
    public void theCustomerRequestsAReportWith(String cid) {
        reportCustomerResult = clientTest.requestReportCustomer(cid);
    }

    @Then("a report is generated for the merchant")
    public void aReportIsGeneratedForTheMerchant() {
        assertNotNull(reportMerchantResult);
    }

    @Then("a report is generated for the customer")
    public void aReportIsGeneratedForTheCustomer() {
        assertNotNull(reportCustomerResult);
    }

    @When("the manager requests a report")
    public void theManagerRequestsAReport() {
        reportManagerResult = clientTest.requestReportManager();
    }

    @Then("a report is generated for the manager")
    public void aReportIsGeneratedForTheManager() {
        assertNotNull(reportManagerResult);
    }

    @When("the customer requests {int} new tokens")
    public void theCustomerRequestsNewTokens(int amount) {
        resultingCustomer.setTokensRequestedAmount(amount);
        tokensList = clientTest.requestTokens(resultingCustomer);
        resultingCustomer.setTokens(tokensList);
    }

    @Then("the customer has {int} tokens")
    public void theCustomerHasTokens(int arg0) {
        assertEquals(arg0,resultingCustomer.getTokens().size());
    }
}
