#!/bin/bash
set -e
docker image prune -f
docker-compose up -d rabbitMq
sleep 10
docker-compose up -d AccountService PaymentService ReportService TokenService RESTService

