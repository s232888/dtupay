#!/bin/bash
set -e

# Build and install the libraries
# abstracting away from using the
# RabbitMq message queue
pushd messaging-utilities-3.4
./build.sh
popd

pushd Models
./build.sh
popd

# Build the services
pushd AccountService
./build.sh
popd 

pushd TokenService
./build.sh
popd

pushd PaymentService
./build.sh
popd

pushd ReportService
./build.sh
popd

pushd RESTService
./build.sh
popd