package account;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import Modules.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.*;
import jakarta.jms.Message;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;


public class AccountServiceTestSteps {
    // MQ
    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();

    private MessageQueue qMock = mock(MessageQueue.class);
    private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
            publishedEvent.complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };
//    private AccountService service = new AccountService(q);
    private CompletableFuture<Customer> registeredCustomerCompletable = new CompletableFuture<>();
    private CompletableFuture<Boolean> unregisteredCustomerCompletable = new CompletableFuture<>();
    //---------




    BankService bank = new BankServiceService().getBankServicePort();
    Customer customer,originalCustomer;
    Merchant merchant;

    AccountService accountservice = new AccountService(q);
    AccountService accountserviceMock = new AccountService(qMock);

    Merchant registeredMerchant;
    boolean registeredCustomer, unregisterMerchantSuccessful,unregisterCustomerSuccessful;
    String customerBankAccount, merchantBankAccount;

    List<String> tokens = new ArrayList<>();
    List<String> initialToken = new ArrayList<>();


    public void generateTokens(){
        for(int i = 0; i < 6; i++){
            initialToken.add(String.valueOf(UUID.randomUUID()));
        }
    }

    @After
    public void after(Scenario scenario) throws BankServiceException_Exception {
        String scenarioname = scenario.getName();

        if (scenarioname.equals("Find BankAccount") || scenarioname.equals("Find Bank accounts for Payment")){
            bank.retireAccount(customerBankAccount);
            bank.retireAccount(merchantBankAccount);
        }
        else if (scenarioname.equals("Unregister merchant")){
            bank.retireAccount(merchantBankAccount);
        }
        else {
            bank.retireAccount(customerBankAccount);
        }


    }


    @Given("a customer with bank account, first name, last name and cpr.")
    public void customerWithBankAccountFirstNameLastNameAndCpr() throws BankServiceException_Exception {
        User userCustomer = new User();
        userCustomer.setFirstName("Daniel");
        userCustomer.setLastName("Test");
        userCustomer.setCprNumber("1709991-1052");
        customerBankAccount = bank.createAccountWithBalance(userCustomer, BigDecimal.valueOf(1000));
        customer = new Customer("Daniel", "Test", "1709991-1052", customerBankAccount);
    }

    @Given("a merchant with bank account, first name, last name and cpr.")
    public void merchantWithBankAccountFirstNameLastNameAndCpr() throws BankServiceException_Exception {
        User userMerchant = new User();
        userMerchant.setFirstName("Michael");
        userMerchant.setLastName("Test");
        userMerchant.setCprNumber("1919191-1052");
        merchantBankAccount = bank.createAccountWithBalance(userMerchant, BigDecimal.valueOf(10000));
        merchant = new Merchant("Michael", "Test", "1919191-1052", merchantBankAccount);
    }


    @When("the customer is registered in DTUPay")
    public void theCustomerIsRegisteredInDTUPay() {
        registeredCustomer = accountservice.registerCustomer(customer);
        accountserviceMock.registerCustomer(customer);
    }

    @When("the merchant is registered in DTUPay")
    public void theMerchantIsRegisteredInDTUPay() {
        registeredMerchant = accountservice.registerMerchant(merchant);
        accountserviceMock.registerMerchant(merchant);
    }

    @Then("the customer is successfully registered")
    public void theCustomerIsSuccessfullyRegistered() {
        assertTrue(registeredCustomer);
    }

    @And("the customer is given tokens")
    public void theCustomerIsGivenTokens() {
        generateTokens();
        customer.setTokens(initialToken);
        customer = accountservice.repository.getCustomerList().get(customer.getId());
        tokens = customer.getTokens();
    }

    @Then("the customer has {int} unique tokens")
    public void theCustomerHasTokens(int numTokens) {
        assertEquals(numTokens,tokens.size());
    }

    @Then("the customer has a valid bank account in the bank")
    public void theCustomerHasAValidBankAccountInTheBank() {
        assertNotNull(customerBankAccount);
    }

    @And("the merchant has a valid bank account in the bank")
    public void theMerchantHasAValidBankAccountInTheBank() {
        assertNotNull(merchantBankAccount);
    }

    @When("the customer has a bank account")
    public void theCustomerHasABankAccount() {
        try {
            customerBankAccount = bank.getAccount(customer.getBankAccNumber()).getId();
        } catch (BankServiceException_Exception e) {
            customerBankAccount = null;
            throw new RuntimeException(e);
        }
    }

    @And("the merchant has a bank account")
    public void theMerchantHasABankAccount() {
        try {
            merchantBankAccount = bank.getAccount(merchant.getBankAccNumber()).getId();
        } catch (BankServiceException_Exception e) {
            merchantBankAccount = null;
            throw new RuntimeException(e);
        }
    }

    @Given("a customer with first name, last name and cpr.")
    public void aCustomerWithFirstNameLastNameAndCpr() {
        customer = new Customer("Daniel", "Test", "971091-1010");
    }

    @Then("the customer registers a bank account")
    public void theCustomerRegistersABankAccount() throws BankServiceException_Exception {


        originalCustomer = accountservice.repository.getCustomerList().get(customer.getId());

        User userCustomer = new User();
        userCustomer.setFirstName(originalCustomer.getFirstName());
        userCustomer.setLastName(originalCustomer.getLastName());
        userCustomer.setCprNumber(originalCustomer.getCpr());
        customerBankAccount = bank.createAccountWithBalance(userCustomer, BigDecimal.valueOf(1000));

        originalCustomer.setBankAccNumber(customerBankAccount);
        accountservice.repository.addCustomerList(originalCustomer.getId(),originalCustomer);
    }

    @When("the customer unregisters from DTUPay")
    public void theCustomerUnregistersFromDTUPay() {
        unregisterCustomerSuccessful = accountservice.unregisterCustomer(customer.getId());
    }

    @Then("the customer is unregistered")
    public void theCustomerIsUnregistered() {
        assertTrue(unregisterCustomerSuccessful);
    }

    @When("the merchant unregisters from DTUPay")
    public void theMerchantUnregistersFromDTUPay() {
        unregisterMerchantSuccessful = accountservice.unregisterMerchant(merchant.getId());
    }

    @Then("the merchant is unregistered")
    public void theMerchantIsUnregistered() {
        assertTrue(unregisterMerchantSuccessful);
    }


    @When("the customer is being registered")
    public void theCustomerIsBeingRegistered() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = accountservice.registerCustomerMQ(customer);
            registeredCustomerCompletable.complete(result);
        }).start();
    }

    @Then("the {string} event is sent")
    public void theEventIsSent(String eventName) {
        System.out.println("HERE");
        Event event = new Event(eventName, new Object[] { customer.getId() });
        assertEquals(event,publishedEvent.join());
    }

    @When("the CustomerTokenAssigned event is sent")
    public void theCustomerTokenAssignedEventIsSent() {
        // This step simulate the event created by a downstream service.
        generateTokens();
        customer.setTokens(initialToken);
        accountservice.handleCustomerTokenAssigned(new Event("..",new Object[] {customer}));
    }

    @Then("the customer is successfully registered with tokens")
    public void theCustomerIsSuccessfullyRegisteredWithTokens() {
        assertEquals(6, registeredCustomerCompletable.join().getTokens().size());
//        assertNotNull(registeredCustomerCompletable.join().getId());
    }

    @Then("the tokens are removed")
    public void theTokensAreRemoved() {
        assertTrue(unregisteredCustomerCompletable.join());
//        assertEquals(0, unregisteredCustomerCompletable.join().getTokens().size());
    }

    @When("the customer is being unregistered")
    public void theCustomerIsBeingUnregistered() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = accountservice.unregisterCustomerMQ(customer);
            unregisteredCustomerCompletable.complete(result);
        }).start();
    }

    @When("the CustomerTokenRemoved event is sent")
    public void theCustomerTokenRemovedEventIsSent() {
        // This step simulate the event created by a downstream service.
        initialToken.clear();
        customer.setTokens(initialToken);
        accountservice.handleCustomerTokenRemoved(new Event("..",new Object[] {true}));
    }

    @When("the FindCustomerBankAccountRequested event is received")
    public void theFindCustomerBankAccountRequestedEventIsReceived() {
        accountserviceMock.handleFindSenderBankAccountRequested(new Event("FindCustomerBankAccountRequested",new Object[] {customer.getId()}));
    }

    @Then("the SenderBankAccountReturned event is sent")
    public void theSenderBankAccountReturnedEventIsSent() {
        Event event = new Event("SenderBankAccountReturned", new Object[] {customerBankAccount});
        verify(qMock).publish(event);
    }

    @Then("the TokenGenerationRequested event is sent")
    public void theTokenGenerationRequestedEventIsSent() {
        Event event = new Event("TokenGenerationRequested", new Object[] { customer });
        assertEquals(event,publishedEvent.join());
    }

    @Then("the TokenRemovalRequested event is sent")
    public void theTokenRemovalRequestedEventIsSent() {
        Event event = new Event("TokenRemovalRequested", new Object[] { customer.getId() });
        assertEquals(event,publishedEvent.join());
    }
}

