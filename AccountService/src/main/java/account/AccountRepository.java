package account;

import Modules.*;

import java.util.*;

public class AccountRepository {

    private HashMap<String,Merchant> merchantList = new HashMap<>();
    private List<Payment> paymentList = new ArrayList<>();

    private HashMap<String,Customer> customerList = new HashMap<>();

    public HashMap<String,Customer> getCustomerList() {
        return customerList;
    }

    public void addCustomerList(String cid, Customer customer) {
        customerList.put(cid,customer);
    }

    public HashMap<String,Merchant> getMerchantList() {
        return merchantList;
    }

    public void addMerchantList(String mid, Merchant merchant) {
        this.merchantList.put(mid, merchant);
    }

    public List<Payment> getPaymentList() {
        return paymentList;
    }

    public void removeCustomerFromList(String cid) {
        this.customerList.remove(cid);
    }

    public void removeMerchantFromList(String mid) {
        this.merchantList.remove(mid);
    }
}
