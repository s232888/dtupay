package account;


import Modules.*;
import dtu.ws.fastmoney.Account;
import messaging.Event;
import messaging.MessageQueue;

import java.util.concurrent.CompletableFuture;

public class AccountService {
//    private Repository repository = new Repository();
    AccountRepository repository = new AccountRepository();
    private CompletableFuture<Customer> registeredCustomer;
    private CompletableFuture<Boolean> unregisteredCustomer;
    private CompletableFuture<Merchant> registeredMerchant;
    MessageQueue queue;

    public AccountService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("CustomerTokenAssigned", this::handleCustomerTokenAssigned);
        this.queue.addHandler("CustomerTokenRemoved", this::handleCustomerTokenRemoved);
        this.queue.addHandler("RegisterCustomerEvent",this ::RegisterCustomerEventHandler);
        this.queue.addHandler("RegisterMerchantEvent",this ::RegisterMerchantEventHandler);
        this.queue.addHandler("UnRegisterCustomerEvent",this ::UnRegisterCustomerEventHandler);
        this.queue.addHandler("UnRegisterMerchantEvent",this ::UnRegisterMerchantEventHandler);
        this.queue.addHandler("FindSenderBankAccountRequested", this::handleFindSenderBankAccountRequested);
        this.queue.addHandler("FindReceiverBankAccountRequested", this::handleFindReceiverBankAccountRequested);
    }


    public Customer registerCustomerMQ(Customer c){
        registeredCustomer = new CompletableFuture<>();
        System.out.println("Token Generation Requested event raised");
        Event event = new Event("TokenGenerationRequested", new Object[] { c });
        queue.publish(event);
        var completedCustomer = registeredCustomer.join();
        registerCustomer(completedCustomer);
        return completedCustomer;
    }

    public void handleCustomerTokenAssigned(Event ev) {
        System.out.println("Customer Token Assigned Event caught");
        var customer = ev.getArgument(0, Customer.class);
        registeredCustomer.complete(customer);
    }



    public boolean registerCustomer(Customer customer){
        repository.addCustomerList(customer.getId(),customer);
        return true;
    }

    public Merchant registerMerchant(Merchant merchant) {
        repository.addMerchantList(merchant.getId(), merchant);
        System.out.println("Merchant added");
        System.out.println(repository.getMerchantList().keySet());
        return repository.getMerchantList().get(merchant.getId());
    }

    public boolean unregisterCustomer(String cid) {
        repository.removeCustomerFromList(cid);
        // TODO: call token service to remove tokens that belong to that customer
        return true;
    }

    public Boolean unregisterCustomerMQ(Customer c){
        unregisteredCustomer = new CompletableFuture<>();
        System.out.println("Token Removal Requested event raised");
        Event event = new Event("TokenRemovalRequested", new Object[] { c.getId() });
        queue.publish(event);
        var completedCustomer = unregisteredCustomer.join();
        unregisterCustomer(c.getId());
        return completedCustomer;
    }

    public void handleCustomerTokenRemoved(Event ev) {
        var customerTokensRemoved = ev.getArgument(0, boolean.class);
        unregisteredCustomer.complete(customerTokensRemoved);
    }

    public void handleFindSenderBankAccountRequested(Event ev) {
        var cid = ev.getArgument(0, String.class);
        var cidBankAccount = repository.getCustomerList().get(cid).getBankAccNumber();
        Event event = new Event("SenderBankAccountReturned", new Object[] { cidBankAccount });
        queue.publish(event);
    }

    public void handleFindReceiverBankAccountRequested(Event ev) {
        System.out.println("Find Receiver BA Requested event caught");
        var mid = ev.getArgument(0, String.class);
        System.out.println("Here in the middle");
        System.out.println(repository.getMerchantList().keySet());
        System.out.println(mid);
        var midBankAccount = repository.getMerchantList().get(mid).getBankAccNumber();
        System.out.println(midBankAccount);
        Event event = new Event("ReceiverBankAccountReturned", new Object[] { midBankAccount });
        queue.publish(event);
    }

    public boolean unregisterMerchant(String mid) {
        repository.removeMerchantFromList(mid);
        return true;
    }


    public void RegisterCustomerEventHandler(Event ev) {
        System.out.println("Register Customer event caught");
        var customer = ev.getArgument(0, Customer.class);
        customer = registerCustomerMQ(customer);
        System.out.println(customer);
        Event event = new Event("RegisterCustomerComplete", new Object[] { customer });
        queue.publish(event);
    }

    public void RegisterMerchantEventHandler(Event ev) {
        System.out.println("Register Merchant event caught");
        var merchant = ev.getArgument(0, Merchant.class);
        merchant = registerMerchant(merchant);
        System.out.println(merchant);
        Event event = new Event("RegisterMerchantComplete", new Object[] { merchant });
        queue.publish(event);
    }

    public void UnRegisterCustomerEventHandler(Event ev) {
        System.out.println("UnRegister Customer event caught");
        var customer = ev.getArgument(0, Customer.class);
        var customerUnregistered = unregisterCustomerMQ(customer);
        System.out.println(customerUnregistered);
        Event event = new Event("UnRegisterCustomerComplete", new Object[] { customerUnregistered });
        queue.publish(event);
    }

    public void UnRegisterMerchantEventHandler(Event ev) {
        System.out.println("UnRegister Merchant event caught");
        var merchant = ev.getArgument(0, Merchant.class);
        var merchantUnregistered = unregisterMerchant(merchant.getId());
        System.out.println(merchantUnregistered);
        Event event = new Event("UnRegisterMerchantComplete", new Object[] { merchantUnregistered });
        queue.publish(event);
    }

}
