Feature: Account administration
  Scenario: Create account
    Given a customer with bank account, first name, last name and cpr.
    When the customer is registered in DTUPay
    Then the customer is successfully registered

  Scenario: Generate tokens
    Given a customer with bank account, first name, last name and cpr.
    When the customer is registered in DTUPay
    And the customer is given tokens
    Then the customer has 6 unique tokens

  Scenario: Find BankAccount
    Given a customer with bank account, first name, last name and cpr.
    And a merchant with bank account, first name, last name and cpr.
    When the customer has a bank account
    And the merchant has a bank account
    Then the customer has a valid bank account in the bank
    And the merchant has a valid bank account in the bank

  Scenario: Create account without bank account
    Given a customer with first name, last name and cpr.
    When the customer is registered in DTUPay
    Then the customer is successfully registered
    Then the customer registers a bank account

  Scenario: Unregister customer
    Given a customer with bank account, first name, last name and cpr.
    And the customer is registered in DTUPay
    When the customer unregisters from DTUPay
    Then the customer is unregistered

  Scenario: Unregister merchant
    Given a merchant with bank account, first name, last name and cpr.
    And the merchant is registered in DTUPay
    When the merchant unregisters from DTUPay
    Then the merchant is unregistered

  Scenario: Register account and generate tokens
    Given a customer with bank account, first name, last name and cpr.
    When the customer is being registered
    Then the TokenGenerationRequested event is sent
    When the CustomerTokenAssigned event is sent
    Then the customer is successfully registered with tokens

  Scenario: Delete tokens and unregister customer
    Given a customer with bank account, first name, last name and cpr.
    And the customer is registered in DTUPay
    When the customer is being unregistered
    Then the TokenRemovalRequested event is sent
    When the CustomerTokenRemoved event is sent
    Then the tokens are removed
    When the customer unregisters from DTUPay
    Then the customer is unregistered

  Scenario: Find Bank accounts for Payment
    Given a customer with bank account, first name, last name and cpr.
    Given a merchant with bank account, first name, last name and cpr.
    When the customer is registered in DTUPay
    When the merchant is registered in DTUPay
    When the FindCustomerBankAccountRequested event is received
    Then the SenderBankAccountReturned event is sent






