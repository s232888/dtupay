Feature: Report Management
  Scenario: Successful payment report updated
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    When the transaction is registered in the report
    Then the merchant report is updated
    And the customer report is updated

  Scenario: Show manager report
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    When the transaction is registered in the report
    And the manager requests a report
    Then the manager report is updated

  Scenario: register a payment for report request
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    When the payment registered event is call the report update
    Then the merchant report is updated

    Scenario: call report for a customer
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    And the payment registered event is call the report update
    When the customer report is requested
    Then the customer report is returned

    Scenario: call report for a merchant
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    And the payment registered event is call the report update
    When the merchant report is requested
    Then the merchant report is returned

    Scenario: call report for a manager
    Given a merchant with id "mid1"
    And a customer with id "cid1"
    And a valid payment of "1000"
    And the payment registered event is call the report update
    When the manager report is requested
    Then the manager report is returned




