package report;


import Modules.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.List;

public class ReportService {

    ReportRepository reportRepository = new ReportRepository();
    MessageQueue queue;

    public ReportService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("ReportPaymentRequested", this::handleReportPaymentRequested);
        this.queue.addHandler("GenerateMerchantReport", this::handlMerchantReportRequested);
        this.queue.addHandler("GenerateCustomerReport", this::handleCustomerReportRequested);
        this.queue.addHandler("GenerateManagerReport", this::handleManagerReportRequested);
    }

    public void handleReportPaymentRequested(Event event) {
        System.out.println("Report Payment requested event caught");
        var cid = event.getArgument(0, String.class);
        var mid = event.getArgument(1, String.class);
        var payment = event.getArgument(2, Payment.class);
        var result = registerPayment(cid,mid,payment);


        Event ev = new Event("PaymentRegisteredInReport", new Object[] { result });
        queue.publish(ev);
    }

    public void handlMerchantReportRequested(Event ev) {
        var mid = ev.getArgument(0, String.class);
        Report report = getMerchantPaymentReport(mid);
        Event event = new Event("MerchantReportGenerated", new Object[] { report });
        queue.publish(event);
    }

    public void handleCustomerReportRequested(Event ev) {
        var cid = ev.getArgument(0, String.class);
        Report report = getCustomerPaymentReport(cid);
        Event event = new Event("CustomerReportGenerated", new Object[] { report });
        queue.publish(event);
    }

    public void handleManagerReportRequested(Event ev) {
        List<Payment> reportList = getManagerPaymentReport();
        Event event = new Event("ManagerReportGenerated", new Object[] { reportList });
        queue.publish(event);
    }

    public boolean registerPayment(String cid, String mid, Payment payment){
        reportRepository.addToMerchantReport(mid,payment);
        reportRepository.addToCustomerReport(cid,payment);
        return true;
    }

    public Report getMerchantPaymentReport(String mid){
        return reportRepository.getMerchantReport(mid);
    }

    public Report getCustomerPaymentReport(String cid) {
        return reportRepository.getCustomerReport(cid);
    }

    public List<Payment> getManagerPaymentReport(){
        return reportRepository.getManagerReport();
    }
}
