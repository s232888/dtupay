package report;

import Modules.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReportRepository {
    private HashMap<String, List<Payment>> customerReport = new HashMap<>();
    private HashMap<String, List<Payment>> merchantReport = new HashMap<>();

    private List<Payment> managerReport = new ArrayList<>();


    public void addToMerchantReport(String mid, Payment payment){
        List<Payment> paymentList = new ArrayList<>();
        if(merchantReport.containsKey(mid)){
            paymentList = merchantReport.get(mid);
        }
        managerReport.add(payment);
        paymentList.add(payment);
        merchantReport.put(mid,paymentList);
    }


    public Report getMerchantReport(String mid) {
        if(merchantReport.containsKey(mid)){
            return new Report(merchantReport.get(mid));
        }
        return new Report();
    }

    public void addToCustomerReport(String cid, Payment payment){
        List<Payment> paymentList = new ArrayList<>();
        if(customerReport.containsKey(cid)){
            paymentList = customerReport.get(cid);
        }
        paymentList.add(payment);
        customerReport.put(cid,paymentList);
    }

    public Report getCustomerReport(String cid) {
        if(customerReport.containsKey(cid)){
            return new Report(customerReport.get(cid));
        }
        return new Report();
    }

    public List<Payment> getManagerReport() {
        return managerReport;
    }
}

