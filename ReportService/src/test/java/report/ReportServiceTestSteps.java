package report;
import java.util.HashMap;
import Modules.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import messaging.MessageQueue;

public class ReportServiceTestSteps {
    List<Payment> managerReportList = new ArrayList<>();
    List<Payment> merchantReportList = new ArrayList<>();
    List<Payment> customerReportList = new ArrayList<>();

    Payment payment = new Payment();
    String mid, cid,customerToken;
    MessageQueue q = mock(MessageQueue.class);
    ReportService service = new ReportService(q);
    boolean transactionRegistered;
    int originalMerchantSize, originalManagerSize, originalCustomerSize;

    @And("a valid payment of {string}")
    public void aValidPayment(String amount) {
        customerToken = String.valueOf(UUID.randomUUID());
        payment = new Payment(new BigDecimal(amount),customerToken, mid);
    }

    @Given("a merchant with id {string}")
    public void aMerchantWithId(String mid) {
        this.mid = mid;
    }

    @When("the transaction is registered in the report")
    public void theTransactionIsRegisteredInTheReport() {
        Report merchantReport = service.getMerchantPaymentReport(mid);
        Report customerReport = service.getCustomerPaymentReport(cid);
        managerReportList = service.getManagerPaymentReport();

        originalMerchantSize = merchantReport.getPayments().size();
        originalCustomerSize = customerReport.getPayments().size();
        originalManagerSize = managerReportList.size();

        transactionRegistered = service.registerPayment(cid, mid,payment);

    }

    @Then("the merchant report is updated")
    public void theMerchantReportIsUpdated(){
        Report merchantReport = service.getMerchantPaymentReport(mid);
        int updatedSize = merchantReport.getPayments().size();
        assertEquals(originalMerchantSize+1, updatedSize);
    }

    @And("the customer report is updated")
    public void theCustomerReportIsUpdated() {
        Report customerReport = service.getCustomerPaymentReport(cid);
        int updatedSize = customerReport.getPayments().size();
        assertEquals(originalCustomerSize+1, updatedSize);
    }

    @And("a customer with id {string}")
    public void aCustomerWithId(String cid) {
        this.cid = cid;
    }

    @And("the manager requests a report")
    public void theManagerRequestsAReport() {
        managerReportList = service.getManagerPaymentReport();
    }

    @Then("the manager report is updated")
    public void theManagerReportIsUpdated() {
        int updatedSize = managerReportList.size();
        assertEquals(originalManagerSize+1, updatedSize);
    }

    @When("the payment registered event is call the report update")
    public void thePaymentRegisteredEventIsCalledTheReport() {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        service.handleReportPaymentRequested(new Event("PaymentRegistered", new Object[] {cid,mid,payment}));
//        assertEquals(true, transactionRegistered);
    }

    @When("the merchant report is requested")
    public void theMerchantReportIsRequested() {
        service.handlMerchantReportRequested(new Event("GenerateMerchantReport", new Object[] { mid }));
    }

    @Then("the merchant report is returned")
    public void theMerchantReportIsReturned() {
        assertEquals(true,  service.getMerchantPaymentReport(mid).getPayments().size()  > 0);
    }

    @When("the customer report is requested")
    public void theCustomerReportIsRequested() {
        service.handleCustomerReportRequested(new Event("GenerateCustomerReport", new Object[] { cid }));
    }

    @Then("the customer report is returned")
    public void theCustomerReportIsReturned() {
//        assertEquals(service,customerReportList.join());
    }

    @When("the manager report is requested")
    public void theManagerReportIsRequested() {
       service.handleManagerReportRequested(new Event("GenerateManagerReport", new Object[] { }));
    }

    @Then("the manager report is returned")
    public void theManagerReportIsReturned() {
        assertTrue(service.getManagerPaymentReport().size() > 0);
    }
}
