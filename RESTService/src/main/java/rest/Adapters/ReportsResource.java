package rest.Adapters;

import Modules.Payment;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import Modules.Report;
import rest.DtuPayService;

import java.util.List;

@Path("/reports")
public class ReportsResource {
    DtuPayService service = DtuPayService.getInstance();

    @Path("/{mid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Report generateMerchantReport(@PathParam("mid") String mid) {
        return service.generateMerchantReport(mid);
    }

    @Path("/{cid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Report generateCustomerReport(@PathParam("cid") String cid) {
        return service.generateCustomerReport(cid);
    }

    @Path("/manager")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> generateMerchantReport() {
        return service.generateManagerReport();
    }
}
