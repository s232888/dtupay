package rest.Adapters;

import Modules.Payment;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import rest.DtuPayService;

@Path("/payments")
public class PaymentsResource {

    DtuPayService service = DtuPayService.getInstance();

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Payment sendPayment(Payment payment){
        System.out.println("Payments post endpoint hit, contacting service to send payment.");
        System.out.println(payment);
        System.out.println(payment.getCustomerToken());
        return service.createPayment(payment);
    }
}
