package rest.Adapters;

import Modules.Customer;
import Modules.Payment;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import rest.DtuPayService;

import java.util.List;

@Path("/customers")
public class CustomersResource {

    DtuPayService service = DtuPayService.getInstance();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Customer registerCustomer(Customer customer) {
        System.out.println("Customers post endpoint hit, contacting service to register customer.");
        return service.registerCustomer(customer);
    }

    @Path("/unregister")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean unregisterCustomer(Customer customer) {
        System.out.println("Customers post endpoint hit, contacting service to unregister customer.");
        return service.unregisterCustomer(customer);
    }

    @Path("/tokensRequest")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<String> requestTokens(Customer customer) {
        System.out.println("Customers post endpoint hit, contacting service to request customer tokens.");
        return service.requestTokens(customer);
    }

}
