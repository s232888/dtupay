package rest.Adapters;

import Modules.Merchant;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import rest.DtuPayService;

@Path("/merchants")
public class MerchantsResource {

    DtuPayService service = DtuPayService.getInstance();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Merchant registerMerchant(Merchant merchant) {
        System.out.println("Merchants post endpoint hit, contacting service to register Merchant.");
        return service.registerMerchant(merchant);
    }
    @Path("/unregister")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean unregisterMerchant(Merchant merchant) {
        System.out.println("Merchants post endpoint hit, contacting service to unregister Merchant.");
        return service.unregisterMerchant(merchant);
    }
}
