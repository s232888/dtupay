package rest;

import Modules.*;
import Modules.Payment;
import com.rabbitmq.client.Connection;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.RabbitMqQueue;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class DtuPayService {

    private static DtuPayService instance = null;
    private CompletableFuture<Boolean> paymentCompleted;
    private CompletableFuture<List<String>> customerTokensRequested;
    private CompletableFuture<Customer> registeredCustomer,customerRequested;
    private CompletableFuture<Merchant> registeredMerchant;
    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();

    private MessageQueue queue;
    private CompletableFuture<Boolean> unregisteredCustomer,unregisteredMerchant;
    private CompletableFuture<Report> generatedMerchantReport;
    private CompletableFuture<Report> generatedCustomerReport;
    private CompletableFuture<List<Payment>> generatedManagerReport;

    private DtuPayService() {
    }

    public static DtuPayService getInstance() {
        if (instance == null) {
            instance = new DtuPayService();
            instance.setMessageQueue(new RabbitMqQueue("rabbitMq"));
        }
        return instance;
    }

    public void setMessageQueue(MessageQueue q) {
        queue = q;
        this.queue.addHandler("RegisterCustomerComplete", this::handleRegisterCustomerCompleted);
        this.queue.addHandler("PaymentCompleted", this::handlePaymentCompleted);
        this.queue.addHandler("RegisterMerchantComplete", this::handleRegisterMerchantCompleted);
        this.queue.addHandler("UnRegisterMerchantComplete", this::handleUnRegisterMerchantCompleted);
        this.queue.addHandler("UnRegisterCustomerComplete", this::handleUnRegisterCustomerCompleted);
        this.queue.addHandler("CustomerTokensReturned", this::handleCustomerTokensReturned);
        this.queue.addHandler("MerchantReportGenerated", this::handleMerchantReportGenerated);
        this.queue.addHandler("CustomerReportGenerated", this::handleCustomerReportGenerated);
        this.queue.addHandler("ManagerReportGenerated", this::handleManagerReportGenerated);
    }

    public Payment createPayment(Payment payment) {
        paymentCompleted = new CompletableFuture<>();
        System.out.println("Raising TransferMoneyRequested");
        System.out.println(payment.getCustomerToken());
        Event event = new Event("TransferMoneyRequested", new Object[] { payment });
        queue.publish(event);
        var completedPayment = paymentCompleted.join();
        System.out.println("Payment completed");
        return payment;

    }

    public Customer registerCustomer(Customer customer) {
        registeredCustomer = new CompletableFuture<>();
        System.out.println("Raising RegisterCustomerEvent");
        //System.out.println(customer);
        Event event = new Event("RegisterCustomerEvent", new Object[] { customer });
        queue.publish(event);
        var completedCustomer = registeredCustomer.join();
        System.out.println(completedCustomer.toString());
        return completedCustomer;
    }

    public void handleRegisterCustomerCompleted(Event ev) {
        System.out.println("Customer Token Assigned Event caught");
        var customer = ev.getArgument(0, Customer.class);
        registeredCustomer.complete(customer);
    }

    public Merchant registerMerchant(Merchant merchant) {
        registeredMerchant = new CompletableFuture<>();
        System.out.println("Raising RegisterMerchantEvent");
        Event event = new Event("RegisterMerchantEvent", new Object[] { merchant });
        queue.publish(event);
        var completedMerchant = registeredMerchant.join();
        System.out.println(completedMerchant.toString());
        return completedMerchant;
    }
    public void handleRegisterMerchantCompleted(Event ev) {
        System.out.println("Merchant Register Completed Event caught");
        var merchant = ev.getArgument(0, Merchant.class);
        registeredMerchant.complete(merchant);
    }

    public void handlePaymentCompleted(Event ev) {
        System.out.println("Payment Completed Event caught");
        var paymentResult = ev.getArgument(0, boolean.class);
        paymentCompleted.complete(paymentResult);
    }

    public boolean unregisterMerchant(Merchant merchant) {
        unregisteredMerchant = new CompletableFuture<>();
        System.out.println("Raising UnRegisterMerchantEvent");
        Event event = new Event("UnRegisterMerchantEvent", new Object[] { merchant });
        queue.publish(event);
        var removedMerchant = unregisteredMerchant.join();
        System.out.println(removedMerchant.toString());
        return removedMerchant;
    }

    public boolean unregisterCustomer(Customer customer) {
        unregisteredCustomer = new CompletableFuture<>();
        System.out.println("Raising UnRegisterCustomerEvent");
        //System.out.println(customer);
        Event event = new Event("UnRegisterCustomerEvent", new Object[] { customer });
        queue.publish(event);
        var removedCustomer = unregisteredCustomer.join();
        System.out.println(removedCustomer.toString());
        return removedCustomer;
    }

    public void handleUnRegisterCustomerCompleted(Event ev) {
        System.out.println("Customer Unregister Event caught");
        var customerRemoved = ev.getArgument(0, boolean.class);
        unregisteredCustomer.complete(customerRemoved);
    }

    public void handleUnRegisterMerchantCompleted(Event ev) {
        System.out.println("Merchant Unregister Completed Event caught");
        var merchantRemoved = ev.getArgument(0, boolean.class);
        unregisteredMerchant.complete(merchantRemoved);
    }


    public Report generateMerchantReport(String mid) {
        generatedMerchantReport = new CompletableFuture<>();
        Event event = new Event("GenerateMerchantReport", new Object[] { mid });
        queue.publish(event);
        return generatedMerchantReport.join();
    }

    public Report generateCustomerReport(String cid) {
        generatedCustomerReport = new CompletableFuture<>();
        Event event = new Event("GenerateCustomerReport", new Object[] { cid });
        queue.publish(event);
        return generatedCustomerReport.join();
    }

    public List<Payment> generateManagerReport() {
        generatedManagerReport = new CompletableFuture<>();
        Event event = new Event("GenerateManagerReport", new Object[] {  });
        queue.publish(event);
        return generatedManagerReport.join();
    }

    public void handleMerchantReportGenerated(Event ev) {
        var merchantReport = ev.getArgument(0, Report.class);
        System.out.println("handleMerchantReportGenerated has been called :)");
        generatedMerchantReport.complete(merchantReport);
    }

    public void handleCustomerReportGenerated(Event ev) {
        var CustomerReport = ev.getArgument(0, Report.class);
        System.out.println("handleCustomerReportGenerated has been called :)");
        generatedCustomerReport.complete(CustomerReport);
    }

    public void handleManagerReportGenerated(Event ev) {
        var managerReport = ev.getArgument(0, List.class);
        System.out.println("handleManagerReportGenerated has been called :)");
        generatedManagerReport.complete(managerReport);
    }


    public List<String> requestTokens(Customer customer) {
        customerTokensRequested = new CompletableFuture<>();
        System.out.println("Request Tokens Event raised");
        Event event = new Event("GetCustomerTokens", new Object[] { customer });
        queue.publish(event);
        var customerTokens = customerTokensRequested.join();
        return customerTokens;
    }

    public void handleCustomerTokensReturned(Event ev) {
        System.out.println("Customer Tokens Returned Event caught");
        var customerTokens = ev.getArgument(0, List.class);
        customerTokensRequested.complete(customerTokens);
    }
}
