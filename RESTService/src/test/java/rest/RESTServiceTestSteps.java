package rest;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RESTServiceTestSteps {

    @Given("there is a customer with empty id")
    public void there_is_a_customer_with_empty_id() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @When("the customer is being registered")
    public void the_customer_is_being_registered() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Then("the {string} event is sent")
    public void the_event_is_sent(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @When("the {string} event is sent with non-empty id")
    public void the_event_is_sent_with_non_empty_id(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Then("the customer is registered and his id is set")
    public void the_customer_is_registered_and_his_id_is_set() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
}
