Feature: Payment Management
  Scenario: Successful money transfer
    Given a customer with bank account and balance "1000" and a merchant with bank account and balance "1000"
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment is successful

  Scenario: Unsuccessful money transfer when customer bank account doesnt exist
    Given a customer with incorrect bank account number and a merchant with correct bank account
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment fails

  Scenario: Unsuccessful money transfer when the sender doesnt have the resources for the transfer
    Given a customer with bank account and balance "1000" and a merchant with bank account and balance "1000"
    When the merchant initiates a payment for "1000000" kr by the customer
    Then the payment fails

  Scenario: Unsuccessful money transfer when merchant bank account doesnt exist
    Given a customer with correct bank account and a merchant with invalid bank account
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment fails

  Scenario: Report Successful Payment
    Given a customer with bank account and balance "1000" and a merchant with bank account and balance "1000"
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment is successful
    When the payment is being reported
    Then the ReportPaymentRequested event is sent
    When the PaymentRegisteredInReport event is sent
    Then the report is successfully registered

  Scenario: Report Unsuccessful Payment
    Given a customer with incorrect bank account number and a merchant with correct bank account
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment fails
    When the payment is being reported
    Then the ReportPaymentRequested event is sent
    When the PaymentRegisteredInReport event is sent
    Then the report is successfully registered

  Scenario: Request CID to TokenService
    Given a customer with a token
    When the cid is being requested
    Then the TokenToCIDRequested event is sent
    When the CIDReturned event is sent
    Then the CID is returned successfully

  Scenario: Request Sender Bank Account
    Given a customer with id "cid1"
    When the Sender Bank Account is being requested
    Then the FindSenderBankAccountRequested event is sent
    When the SenderBankAccountReturned event is sent
    Then the Sender Bank Account is returned successfully

  Scenario: Request Receiver Bank Account
    Given a merchant with id "mid1"
    When the Receiver Bank Account is being requested
    Then the FindReceiverBankAccountRequested event is sent
    When the ReceiverBankAccountReturned event is sent
    Then the Receiver Bank Account is returned successfully


  Scenario: Full payment scenario
    Given a customer with bank account and balance "1000" and a merchant with bank account and balance "1000"
    And a payment of "10" kr
    When the Transfer Money Request is being handled
    When the CIDReturned event is sent
    When the ReceiverBankAccountReturned event is sent
    When the SenderBankAccountReturned event is sent
    When the PaymentRegisteredInReport event is sent
    Then the full payment scenario is successful




  # Refund would be the same, just swapping the accounts.