package payment;


import Modules.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

public class PaymentService {
    BankService bank = new BankServiceService().getBankServicePort();
    private CompletableFuture<Boolean> paymentReported;
    private CompletableFuture<String> cidRequested,paymentStep1,paymentStep2,paymentStep3;
    private CompletableFuture<String> senderBankAccountRequested,receiverBankAccountRequested;

    private CompletableFuture<String> cid,midBA,cidBA;

    MessageQueue queue;


    public PaymentService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("PaymentRegisteredInReport", this::handlePaymentRegisteredInReport);
        this.queue.addHandler("CIDReturned", this::handleCIDReturned);
        this.queue.addHandler("SenderBankAccountReturned", this::handleSenderBankAccountReturned);
        this.queue.addHandler("ReceiverBankAccountReturned", this::handleReceiverBankAccountReturned);
        this.queue.addHandler("TransferMoneyRequested", this::handleTransferMoneyRequested);
    }

    // ******************************************* Send events *******************************************

    public String getCIDMQ(String token) {
        System.out.println("getCIDMQ Started");
        System.out.println("getCIDMQ");
        System.out.println(token);
        cidRequested = new CompletableFuture<>();
        Event event = new Event("TokenToCIDRequested", new Object[] { token });
        queue.publish(event);
        System.out.println("TokenToCIDRequested published");
        cidRequested.join();
        System.out.println("getCIDMQ Ended");
        return cidRequested.join();
    }

    public String getSenderBankAccountMQ(String cid) {
        System.out.println("getSenderBankAccount started");
        senderBankAccountRequested = new CompletableFuture<>();
        Event event = new Event("FindSenderBankAccountRequested", new Object[] { cid });
        queue.publish(event);
        System.out.println("FindSenderBankAccountRequested published");
        senderBankAccountRequested.join();
        System.out.println("getSenderBankAccount ended");
        return senderBankAccountRequested.join();
    }

    public String getReceiverBankAccountMQ(String mid) {
        System.out.println("getReceiverBankAccount started");
        receiverBankAccountRequested = new CompletableFuture<>();
        Event event = new Event("FindReceiverBankAccountRequested", new Object[] { mid });
        queue.publish(event);
        System.out.println("FindReceiverBankAccountRequested published");
        receiverBankAccountRequested.join();
        System.out.println("getReceiverBankAccount ended");
        return receiverBankAccountRequested.join();
    }

    public boolean ReportMQ(String cid, String mid, Payment p){
        paymentReported = new CompletableFuture<>();
        Event event = new Event("ReportPaymentRequested", new Object[] {cid,mid, p });
        queue.publish(event);
        System.out.println("ReportPaymentRequested published");
        paymentReported.join();
        System.out.println("ReportMQ ended");
        return paymentReported.join();
    }

    public synchronized boolean transferMoneyRequestMQ(String token, String mid, BigDecimal amount){
        // Send token to Token Service to get CID
        System.out.println("transferMoneyRequestMQ");
        System.out.println(token);
        String sender = getCIDMQ(token);
        System.out.println(sender);
//
//        // Send MID bank account request
        String receiverBankAccount = getReceiverBankAccountMQ(mid);
        System.out.println(receiverBankAccount);
//
//        // Send CID bank account request
        String senderBankAccount = getSenderBankAccountMQ(sender);
        System.out.println(senderBankAccount);
//
//        // Perform actual payment
        Payment p = transferMoney(token,mid,senderBankAccount,receiverBankAccount,amount);
//        Payment p = new Payment(amount,token,mid);

        // Report payment
        boolean result = ReportMQ(sender, mid, p);
//        System.out.println(result);
//        return true;
        return result;
    }


    // ******************************************* Handlers *******************************************
    public void handlePaymentRegisteredInReport(Event e){
        System.out.println("Payment registered in report caught");
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        var paymentAddedToReport = e.getArgument(0, Boolean.class);
        System.out.println("payment added to report result caught");
        paymentReported.complete(paymentAddedToReport);
    }
    public void handleCIDReturned(Event e){
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        var cid = e.getArgument(0, String.class);
        cidRequested.complete(cid);
    }
    public void handleSenderBankAccountReturned(Event e){
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        var senderBankAccount = e.getArgument(0, String.class);
        senderBankAccountRequested.complete(senderBankAccount);
    }
    public void handleReceiverBankAccountReturned(Event e){
        System.out.println("Receiver BA Returned event caught");
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        var receiverBankAccount = e.getArgument(0, String.class);
        receiverBankAccountRequested.complete(receiverBankAccount);
    }

    public void handleTransferMoneyRequested(Event e){
        var payment = e.getArgument(0,Payment.class);
        var token = payment.getCustomerToken();
        System.out.println("handleTransferMoneyRequested");
        System.out.println(token);
        var mid = payment.getMid();
        var amount = payment.getAmount();

        var transactionResult = transferMoneyRequestMQ(token,mid,amount);

        Event event = new Event("PaymentCompleted", new Object[] { transactionResult });
        queue.publish(event);
    }


    // **************** Business logic **********************


    public Payment transferMoney(String token, String mid, String senderBankAccountNumber, String receiverBankAccountNumber, BigDecimal amount) {
        System.out.println("Transfer Money started");
        String message = amount + " kr transferred from " + senderBankAccountNumber + " to " + receiverBankAccountNumber;
        try{
            bank.transferMoneyFromTo(senderBankAccountNumber,receiverBankAccountNumber,amount, message);
            Payment payment = new Payment(amount,token,mid);
            System.out.println("Payment successful");
            return payment;
        }
        catch (BankServiceException_Exception e){
            System.out.println(e.getMessage());
            System.out.println("Payment unsuccessful");
            return new Payment(new BigDecimal("0"),token,mid);
        }

    }


}
