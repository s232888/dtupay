package payment;

import Modules.Customer;
import Modules.Payment;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import io.cucumber.java.After;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class PaymentServiceTestSteps {
    String customerBankAccount, merchantBankAccount;
    BigDecimal amount;
    Event eventSent = new Event();

    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
    private CompletableFuture<Event> publishedEvent2 = new CompletableFuture<>();
    private CompletableFuture<Boolean> paymentReportedCompletable = new CompletableFuture<>();
    private CompletableFuture<Boolean> transferMoneyRequested = new CompletableFuture<>();
    private CompletableFuture<String> cidRequested = new CompletableFuture<>();
    private CompletableFuture<String> senderBankAccountRequested = new CompletableFuture<>();
    private CompletableFuture<String> receiverBankAccountRequested = new CompletableFuture<>();
//
    private CompletableFuture<List<String>> bankAccountsRequested = new CompletableFuture<>();
    private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
//            CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
            publishedEvent.complete(event);
//            eventSent = event;
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }
    };

    PaymentService paymentService = new PaymentService(q);
    boolean paymentSuccessful;
    BankService bank = new BankServiceService().getBankServicePort();
    User userCustomer = new User();
    User userMerchant = new User();
    Payment payment;
    String token;
    String mid;
    String cid;


    @Before
    public void before(){
        userCustomer.setFirstName("Daniel");
        userCustomer.setLastName("Test");
        userCustomer.setCprNumber("871777091-1010");

        userMerchant.setFirstName("Nir");
        userMerchant.setLastName("Test");
        userMerchant.setCprNumber("017777719-1010");
    }



    @After
    public void after(Scenario scenario) throws BankServiceException_Exception {
        String scenarioName = scenario.getName();
        if (scenarioName.equals("Unsuccessful money transfer when customer bank account doesnt exist") || scenarioName.equals("Report Unsuccessful Payment")){
            bank.retireAccount(merchantBankAccount);
        }
        else if(scenarioName.equals("Unsuccessful money transfer when merchant bank account doesnt exist")){
            bank.retireAccount(customerBankAccount);
        }
        else if(scenarioName.equals("Request CID to TokenService") ||
                scenarioName.equals("Request Sender Bank Account") ||
                scenarioName.equals("Request Receiver Bank Account")){

        }
        else {
            bank.retireAccount(customerBankAccount);
            bank.retireAccount(merchantBankAccount);
        }

    }

    @Given("a customer with bank account and balance {string} and a merchant with bank account and balance {string}")
    public void aCustomerWithBankAccountAndAMerchantWithBankAccount(String customerBalance, String merchantBalance)
            throws BankServiceException_Exception {
        customerBankAccount = bank.createAccountWithBalance(userCustomer, new BigDecimal(customerBalance));
        merchantBankAccount = bank.createAccountWithBalance(userMerchant, new BigDecimal(merchantBalance));
    }

    @When("the merchant initiates a payment for {string} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(String amount) throws BankServiceException_Exception {
        token = String.valueOf(UUID.randomUUID());
        mid = "mid1";
        payment = paymentService.transferMoney(token, mid, customerBankAccount,merchantBankAccount, new BigDecimal(amount));
//        System.out.println(payment.getAmount().intValue() != 0);
        paymentSuccessful = payment.getAmount().intValue() != 0;
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(paymentSuccessful);
    }

    @Given("a customer with incorrect bank account number and a merchant with correct bank account")
    public void aCustomerWithIncorrectBankAccountNumberAndAMerchantWithCorrectBankAccount() throws BankServiceException_Exception {
        customerBankAccount = "Non existent account";
        merchantBankAccount = bank.createAccountWithBalance(userMerchant, BigDecimal.valueOf(1000));
    }

    @Then("the payment fails")
    public void thePaymentFails() {
        assertFalse(paymentSuccessful);
    }

    @Given("a customer with correct bank account and a merchant with invalid bank account")
    public void aCustomerWithCorrectBankAccountAndAMerchantWithInvalidBankAccount() throws BankServiceException_Exception {
        merchantBankAccount = "Non existent account";
        customerBankAccount = bank.createAccountWithBalance(userCustomer, BigDecimal.valueOf(1000));
    }

    @When("the payment is being reported")
    public void thePaymentIsBeingReported() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = paymentService.ReportMQ(cid,mid,payment);
            paymentReportedCompletable.complete(result);
        }).start();
    }

    @When("the cid is being requested")
    public void theCidIsBeingRequested() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = paymentService.getCIDMQ(token);
            cidRequested.complete(result);
        }).start();
    }

    @Then("the ReportPaymentRequested event is sent")
    public void theReportPaymentRequestedEventIsSent() {
        Event event = new Event("ReportPaymentRequested", new Object[] {cid,mid, payment });
        assertEquals(event,publishedEvent.join());
    }
    @Then("the TokenToCIDRequested event is sent")
    public void theTokenToCIDRequestedEventIsSent() {
        Event event = new Event("TokenToCIDRequested", new Object[] { token });
        assertEquals(event,publishedEvent.join());
    }


    @When("the PaymentRegisteredInReport event is sent")
    public void thePaymentRegisteredInReportEventIsSent() {
        // This step simulate the event created by a downstream service.
        paymentService.handlePaymentRegisteredInReport(new Event("..",new Object[] {true}));
    }
    @When("the CIDReturned event is sent")
    public void theCIDReturnedEventIsSent() {
        String cidRequested = "cid1";
        paymentService.handleCIDReturned(new Event("..",new Object[] {cidRequested}));
        System.out.println("Response for CID Request sent");
    }

    @Then("the report is successfully registered")
    public void theReportIsSuccessfullyRegistered() {
        assertTrue(paymentReportedCompletable.join());
    }
    @Then("the CID is returned successfully")
    public void theCIDIsReturnedSuccessfully() {
        assertEquals("cid1",cidRequested.join());
    }

    @Given("a customer with a token")
    public void aCustomerWithAToken() {
        token = String.valueOf(UUID.randomUUID());
    }


    @Given("a customer with id {string}")
    public void aCustomerWithId(String cid) {
        this.cid = cid;
    }

    @And("a merchant with id {string}")
    public void aMerchantWithId(String mid) {
        this.mid = mid;
    }

    @When("the Sender Bank Account is being requested")
    public void theSenderBankAccountIsBeingRequested() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = paymentService.getSenderBankAccountMQ(cid);
            senderBankAccountRequested.complete(result);
        }).start();
    }

    @Then("the FindSenderBankAccountRequested event is sent")
    public void theFindSenderBankAccountRequestedEventIsSent() {
        Event event = new Event("FindSenderBankAccountRequested", new Object[] { "cid1" });
        assertEquals(event,publishedEvent.join());
    }

    @When("the SenderBankAccountReturned event is sent")
    public void theSenderBankAccountReturnedEventIsSent() {
        // This step simulate the event created by a downstream service.
        paymentService.handleSenderBankAccountReturned(new Event("..",new Object[] {customerBankAccount}));
    }

    @Then("the Sender Bank Account is returned successfully")
    public void theSenderBankAccountIsReturnedSuccessfully() {
        assertEquals(customerBankAccount, senderBankAccountRequested.join());
    }

    @When("the Receiver Bank Account is being requested")
    public void theReceiverBankAccountIsBeingRequested() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = paymentService.getReceiverBankAccountMQ(mid);
            receiverBankAccountRequested.complete(result);
        }).start();
    }

    @Then("the FindReceiverBankAccountRequested event is sent")
    public void theFindReceiverBankAccountRequestedEventIsSent() {
        Event event = new Event("FindReceiverBankAccountRequested", new Object[] { "mid1" });
        assertEquals(event, publishedEvent.join());
    }

    @When("the ReceiverBankAccountReturned event is sent")
    public void theReceiverBankAccountReturnedEventIsSent() {
        // This step simulate the event created by a downstream service.
        System.out.println("Event is sent");
        paymentService.handleReceiverBankAccountReturned(new Event("..",new Object[] {merchantBankAccount}));
    }

    @Then("the Receiver Bank Account is returned successfully")
    public void theReceiverBankAccountIsReturnedSuccessfully() {
        assertEquals(merchantBankAccount, receiverBankAccountRequested.join());
    }


    @And("a payment of {string} kr")
    public void aPaymentOfKr(String amount) {
        this.amount = new BigDecimal(amount);
    }

    @When("the Transfer Money Request is being handled")
    public void theTransferMoneyRequestIsBeingHandled() {
        // We have to run the registration in a thread, because
        // the register method will only finish after the next @When
        // step is executed.
        new Thread(() -> {
            var result = paymentService.transferMoneyRequestMQ(mid,token,amount);
            transferMoneyRequested.complete(result);
        }).start();
    }

    @Then("the full payment scenario is successful")
    public void theFullPaymentScenarioIsSuccessful() {
        assertTrue(transferMoneyRequested.join());
    }
}
