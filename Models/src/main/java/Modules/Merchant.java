package Modules;

import java.util.UUID;

public class Merchant {
    private String id;
    private String cpr;
    private String bankAccNumber;
    private String firstName;
    private String lastName;


    public Merchant() {
        this.id = String.valueOf(UUID.randomUUID());
    }

    public Merchant(String firstName, String lastName, String cpr, String bankAccNumber) {
        this.id = String.valueOf(UUID.randomUUID());
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
        this.bankAccNumber = bankAccNumber;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }


    public String getBankAccNumber() {
        return bankAccNumber;
    }

    public void setBankAccNumber(String number) {
        this.bankAccNumber = number;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String first_name) {
        this.firstName = first_name;
    }
    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

}
