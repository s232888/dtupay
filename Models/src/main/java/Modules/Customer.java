package Modules;

import java.util.List;
import java.util.UUID;

public class Customer {
    private String id;
    private String cpr;
    private String bankAccNumber;
    private String firstName;
    private String lastName;
    private Integer tokensRequestedAmount;


    private List<String> tokens;

    public Customer() {
        this.id = String.valueOf(UUID.randomUUID());
    }

    public Customer(String firstName, String lastName, String cpr) {
        this.id = String.valueOf(UUID.randomUUID());
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
        this.bankAccNumber = "";

    }

    public Customer(String firstName, String lastName, String cpr, String backAccNumber) {
        this.id = String.valueOf(UUID.randomUUID());
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
        this.bankAccNumber = backAccNumber;
    }


    public String getId() {
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }


    public String getBankAccNumber() {
        return bankAccNumber;
    }

    public void setBankAccNumber(String bankAccNumber) {
        this.bankAccNumber = bankAccNumber;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String first_name) {
        this.firstName = first_name;
    }
    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        if(tokens.size()<=6) {
            this.tokens = tokens;
        }
    }

    public String useToken(){
        String token = tokens.get(0);
        tokens.remove(0);
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Customer)) {
            return false;
        }
        var c = (Customer) o;
        return cpr != null && cpr.equals(c.getCpr()) ||
                        cpr == null && c.getCpr() == null;
    }

    @Override
    public int hashCode() {
        return cpr == null ? 0 : cpr.hashCode();
    }

    @Override
    public String toString() {
        return String.format("Customer with name %s and cpr %s.", getFirstName() + getLastName(), cpr);
    }

    public Integer getTokensRequestedAmount() {
        return tokensRequestedAmount;
    }

    public void setTokensRequestedAmount(Integer tokensRequestedAmount) {
        this.tokensRequestedAmount = tokensRequestedAmount;
    }
}

