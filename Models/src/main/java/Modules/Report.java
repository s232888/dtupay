package Modules;

import java.util.ArrayList;
import java.util.List;

public class Report {
    // Maybe the Report should be a hashmap for each payment connected to a customer/merchant

    private List<Payment> payments;

    public Report(){
        payments = new ArrayList<>();
    }

    public Report(List<Payment> paymentList){
        this.payments = paymentList;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void add(Payment p){
        payments.add(p);
    }
}
