package Modules;
import java.util.UUID;

public class Token {
    private final String value;

    public Token() {
        this.value = String.valueOf(UUID.randomUUID()); // TODO: Update to use TokenService
    }

    public String getValue() {
        return value;
    }



}
