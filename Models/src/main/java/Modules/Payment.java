package Modules;

import java.math.BigDecimal;
import java.util.UUID;

public class Payment {
    private BigDecimal amount;
    private String paymentId,customerToken,mid;

    public Payment(){};

    public Payment(BigDecimal amount,String customerToken, String mid){
        this.amount = amount;
        this.customerToken = customerToken;
        this.mid = mid;
        this.paymentId = String.valueOf(UUID.randomUUID());
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }
}
